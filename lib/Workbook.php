<?php
namespace XLSXLight;

use Exception;
use ZipArchive;

/**
 * Class XLSXLight
 * @property string $WorkbookAuthor
 * @property ObjectIterator $sheets
 * @property integer $activeSheet
 * @property ObjectIterator $sharedStrings
 * @property integer $sharedStringsCount
 * @property array $sharedImages
 * @const COLUMN_UNIT_RATIO
 */
class Workbook extends Style
{
    private $WorkbookAuthor = 'DOC Author';
    public $sheets;
    private $activeSheet = 1;
    private $sharedStrings;
    private $sharedStringsCount = 0;
    private $sharedImages = [];
    const COLUMN_UNIT_RATIO = 6;

    function __construct()
    {
        $this->sheets = new ObjectIterator();
        $this->sharedStrings = new ObjectIterator();

        $this->initStyles();
    }

    public function setAuthor($authorName)
    {
        $this->WorkbookAuthor = $authorName;
        return $this;
    }

    public function setActiveSheet($sheetId)
    {
        $this->activeSheet = $sheetId;
        return $this;
    }

    public static function getColumnLetter($number)
    {
        for ($letter = ""; $number >= 0; $number = intval($number / 26) - 1) {
            $letter = chr($number % 26 + 0x41) . $letter;
        }
        return $letter;
    }

    public static function getColumnNumber($columnLetter)
    {
        $columnLetters = str_split(strtoupper($columnLetter));
        $alphabet = array_flip(range('A', 'Z'));
        $value = 0;
        foreach (array_reverse($columnLetters) as $index => $letter) {
            $alphaIndex = $alphabet[$letter] + 1;
            $value += $alphaIndex * pow(26, $index);
        }
        return $value - 1;
    }

    public static function getCellPosition($cellTag)
    {
        preg_match("/^([A-Z]+)([0-9]+)$/", $cellTag, $match);
        if (sizeof($match) != 3) {
            throw new Exception('Invalid Cell Index "' . $cellTag . '"!');
        }
        return [
            'row' => $match[2],
            'col' => Workbook::getColumnNumber($match[1])
        ];
    }

    public function getActiveSheet()
    {
        return $this->activeSheet;
    }

    public static function xmlSpecialChars($val)
    {
        static $badchars = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x7f";
        static $goodchars = "                                 ";
        return strtr(htmlspecialchars($val, ENT_QUOTES | ENT_XML1), $badchars, $goodchars);
    }

    public function addSharedString($string)
    {
        $this->sharedStringsCount++;
        if (!$this->sharedStrings->hasItem($string)) {
            $this->sharedStrings->addItem($this->sharedStrings->count(), $string);
        }
        return $this->sharedStrings->getItem($string);
    }

    private function addSharedImage($filePath)
    {
        $existing = array_search($filePath, $this->sharedImages);
        if ($existing) {
            return $existing;
        }
        $index = count($this->sharedImages) + 1;
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        $this->sharedImages['image' . $index . '.' . $ext] = $filePath;
        return 'image' . $index . '.' . $ext;
    }

    private function generateCoreXml()
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
            . '<dcterms:created xsi:type="dcterms:W3CDTF">' . date('Y-m-d\TH:i:s.00\Z') . '</dcterms:created>'
            . '<dc:creator>' . $this->WorkbookAuthor . '</dc:creator>'
            . '<cp:revision>0</cp:revision>'
            . '</cp:coreProperties>';
    }

    private function generateAppXml()
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">'
            . '<TotalTime>0</TotalTime>'
            . '</Properties>';
    }

    private function generateContentTypes()
    {
        $sheets = '';
        $drawings = '';
        foreach ($this->sheets as $sheet) {
            $sheets .= '<Override PartName="/xl/worksheets/sheet' . $sheet->getId() . '.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"/>';
            if (!empty($sheet->getImages())) {
                $drawings .= '<Override PartName="/xl/drawings/drawing' . $sheet->getId() . '.xml" ContentType="application/vnd.openxmlformats-officedocument.drawing+xml"/>';
            }
        }
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">'
            . '<Default Extension="xml" ContentType="application/xml"/>'
            . '<Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml"/>'
            . '<Default Extension="jpeg" ContentType="image/jpeg"/><Default Extension="png" ContentType="image/png"/><Default Extension="jpg" ContentType="application/octet-stream"/>'
            . '<Override PartName="/xl/workbook.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml"/>'
            . $sheets
            . '<Override PartName="/xl/styles.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml"/>'
            . $drawings
            . '<Override PartName="/xl/sharedStrings.xml" ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml"/>'
            . '<Override PartName="/docProps/core.xml" ContentType="application/vnd.openxmlformats-package.core-properties+xml"/>'
            . '<Override PartName="/docProps/app.xml" ContentType="application/vnd.openxmlformats-officedocument.extended-properties+xml"/>'
            . '</Types>';
    }

    private function generateRels()
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">'
            . '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>'
            . '<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>'
            . '<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="xl/workbook.xml"/>'
            . '</Relationships>';
    }

    private function generateWorkbookXml()
    {
        $sheets = '';
        foreach ($this->sheets as $sheet) {
            $sheets .= '<sheet name="' . $sheet->getTitle() . '" sheetId="' . $sheet->getId() . '" state="visible" r:id="rId' . $sheet->getId() . '"/>';
        }
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n"
            . '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">'
            . '<fileVersion appName="Calc"/><workbookPr backupFile="false" showObjects="all" date1904="false"/><workbookProtection/>'
            . '<bookViews><workbookView activeTab="0" firstSheet="0" showHorizontalScroll="true" showSheetTabs="true" showVerticalScroll="true" tabRatio="212" windowHeight="8192" windowWidth="16384" xWindow="0" yWindow="0"/></bookViews>'
            . '<sheets>' . $sheets . '</sheets>'
            . '<calcPr iterateCount="100" refMode="A1" iterate="false" iterateDelta="0.001"/>'
            . '</workbook>';
    }

    private function generatExRels()
    {
        $i = 1;
        $sheets = '';
        foreach ($this->sheets as $sheet) {
            $sheets .= '<Relationship Id="rId' . $i++ . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet" Target="worksheets/sheet' . $sheet->getId() . '.xml"/>';
        }
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">'
            . $sheets
            . '<Relationship Id="rId' . $i++ . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" Target="sharedStrings.xml"/>'
            . '<Relationship Id="rId' . $i . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>'
            . '</Relationships>';
        return $xml;
    }

    private function generateSharedStrings()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $xml .= '<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="' . $this->sharedStringsCount . '" uniqueCount="' . $this->sharedStrings->count() . '">';
        foreach ($this->sharedStrings as $string => $index) {
            $xml .= '<si><t>' . $string . '</t></si>';
        }
        $xml .= '</sst>';
        return $xml;
    }

    private function generateSheetRels(Sheet $sheet)
    {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">'
            . '<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing" Target="../drawings/drawing' . $sheet->getId() . '.xml"/>'
            . '</Relationships>';
    }

    private function generateSheetDrawings(Sheet $sheet)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<xdr:wsDr xmlns:xdr="http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">';
        /** @var Image $image */
        foreach ($sheet->getImages() as $image) {
            $xml .= $image->generateXml();
        }
        $xml .= '</xdr:wsDr>';
        return $xml;
    }

    private function generateDrawingRels(Sheet $sheet)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            . '<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">';
        /** @var  Image $image */
        foreach ($sheet->getImages() as $image) {
            $xml .= '<Relationship Id="rId' . $image->getId() . '" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image" Target="../media/' . $image->getXlsImage() . '"/>';
        }
        $xml .= '</Relationships>';
        return $xml;
    }

    public function save($filename)
    {
        @unlink($filename);
        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE) != true) {
            die ("Could not open archive");
        }
        $zip->addFromString('docProps/core.xml', $this->generateCoreXml());
        $zip->addFromString('docProps/app.xml', $this->generateAppXml());
        $zip->addFromString('[Content_Types].xml', $this->generateContentTypes());
        $zip->addFromString('_rels/.rels', $this->generateRels());
        $zip->addFromString('xl/workbook.xml', $this->generateWorkbookXml());
        $zip->addFromString('xl/_rels/workbook.xml.rels', $this->generatExRels());

        /** @var Sheet $sheet */
        foreach ($this->sheets as $sheet) {

            $zip->addFile($sheet->getSheetFile(), 'xl/worksheets/sheet' . $sheet->getId() . '.xml');
            if (!empty($sheet->getImages())) {
                $sheet->adjustImagePositions();
                /** @var Image $image */
                foreach ($sheet->getImages() as $image) {
                    $sharedImage = $this->addSharedImage($image->getOriginImage());
                    //$zip->addFromString('xl/media/' . $sharedImage, $image->getRawImage());
                    $zip->addFile($image->getOriginImage(),'xl/media/' . $sharedImage);
                    $image->setSharedImage($sharedImage);
                }
                $zip->addFromString('xl/worksheets/_rels/sheet' . $sheet->getId() . '.xml.rels', $this->generateSheetRels($sheet));
                $zip->addFromString('xl/drawings/drawing' . $sheet->getId() . '.xml', $this->generateSheetDrawings($sheet));
                $zip->addFromString('xl/drawings/_rels/drawing' . $sheet->getId() . '.xml.rels', $this->generateDrawingRels($sheet));
            }

        }

        $zip->addFromString('xl/styles.xml', $this->generateStylesXml());
        if (!empty($this->sharedStrings->count())) {
            $zip->addFromString('xl/sharedStrings.xml', $this->generateSharedStrings());
        }

        $zip->close();
    }

    public function stdOut()
    {
        $filename = tempnam("/tmp", "xlsxlight_workbook");
        $this->save($filename);
        readfile($filename);
    }
}