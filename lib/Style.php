<?php
namespace XLSXLight;

use Exception;
use stdClass;

/**
 * @property Border[] $borders;
 */
class Style
{
    private $formatIndex = 165;
    private $fillIndex = 2;

    private $fonts = [];
    private $styleSet = [];
    private $numFormat = [];
    private $borders = [];
    private $fills = [];
    private $slashedChars = ' ';

    protected function initStyles()
    {
        //add default font
        $this->setFont(new Font('default'));

        //add default number formats
        $this->setNumberFormat('default', '#,##0.00');
        $this->setNumberFormat('currency', '#,##0.00 "€"');
        $this->setNumberFormat('datetime', 'dd/mm/yyyy hh:mm;@');
        $this->setNumberFormat('date', 'dd/mm/yyyy;@');
        $this->setNumberFormat('time', 'hh:mm:ss;@');
        $this->setNumberFormat('percent', '#,##0.00 %');
    }

    public function setFont(Font $font)
    {
        $this->fonts[$font->getTag()] = $font;
        return $this;
    }

    public function setNumberFormat($formatTag, $format)
    {
        if (isset($this->numFormat[$formatTag])) {
            $formatOb = $this->numFormat[$formatTag];
        } else {
            $formatOb = new stdClass();
            $formatOb->id = $this->formatIndex++;
        }
        $formatOb->format = addcslashes($format, $this->slashedChars);
        $this->numFormat[$formatTag] = $formatOb;
        return $this;
    }

    public function setFill($fillTag, $rgbColor)
    {
        if (isset($this->fills[$fillTag])) {
            $fill = $this->fills[$fillTag];
        } else {
            $fill = new stdClass();
            $fill->id = $this->fillIndex++;
        }
        $fill->color = ltrim($rgbColor, '#');
        $this->fills[$fillTag] = $fill;
        return $this;
    }

    public function setBorder($borderTag, $styles, $colors)
    {
        $id = count($this->borders);
        if (!isset($this->borders[$borderTag])) {
            $id++;
        }
        $this->borders[$borderTag] = (new Border($styles, $colors))->setId($id);
        return $this;
    }

    public function getStyle($font, $numfmt, $fill, $border)
    {
        $fontId = $this->getFontId(strval($font));
        $formatId = (!$numfmt) ? 0 : $this->getFormatId($numfmt);
        $fillId = $this->getFillId($fill);
        $borderId = (!$border) ? 0 : $this->getBorderId($border);
        $alignment = $this->fonts[$font]->getAlignment();
        $horAlign = (isset($alignment['horizontal']) ? $alignment['horizontal'] : 0);
        $verAlign = (isset($alignment['vertical']) ? $alignment['vertical'] : 0);

        $index = $formatId . '_' . $fontId . '_' . $fillId . '_' . $borderId . '_' . $horAlign . '_' . $verAlign;
        if (isset($this->styleSet[$index])) {
            $value = $this->styleSet[$index]->id;
        } else {
            $value = (count($this->styleSet) + 1);//increment count by 1, 0 is taken by default style;
            $style = new stdClass();
            $style->id = $value;
            $style->nformat = $formatId;
            $style->font = $fontId;
            $style->fill = $fillId;
            $style->border = $borderId;
            $style->alignment = $alignment;
            $this->styleSet[$index] = $style;
        }
        return $value;
    }

    public function getFontId($fontTag)
    {
        $fontId = array_search($fontTag, array_keys($this->fonts));
        if ($fontId === false) {
            throw new Exception('Missing font for ' . $fontTag);
        }
        return $fontId;
    }

    public function getFillId($fillTag)
    {
        return isset($this->fills[$fillTag]) ? $this->fills[$fillTag]->id : 0;
    }

    public function getFormatId($numFmt)
    {
        return isset($this->numFormat[$numFmt]) ? $this->numFormat[$numFmt]->id : 0;
    }

    public function getBorderId($border)
    {
        return isset($this->borders[$border]) ? $this->borders[$border]->getId() : 0;
    }


    protected function generateStylesXml()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $xml .= '<styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"'
            . ' mc:Ignorable="x14ac" xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac">';
        if (!empty($this->numFormat)) {
            $xml .= '<numFmts count="' . count($this->numFormat) . '">';
            foreach ($this->numFormat as $format) {
                $xml .= '<numFmt numFmtId="' . $format->id . '" formatCode="' . Workbook::xmlSpecialChars($format->format) . '"/>';
            }
            $xml .= '</numFmts>';
        }
        $xml .= '<fonts count="' . count($this->fonts) . '" x14ac:knownFonts="1">';
        foreach ($this->fonts as $font) {
            $xml .= $font->generateStyle();
        }
        $xml .= '</fonts>';
        $xml .= '<fills count="' . (count($this->fills) + 2) . '">';
        $xml .= '<fill><patternFill patternType="none"/></fill>';
        $xml .= '<fill><patternFill patternType="gray125"/></fill>';
        foreach ($this->fills as $fill) {
            $xml .= '<fill><patternFill patternType="solid"><fgColor rgb="' . strval($fill->color) . '"/><bgColor indexed="64"/></patternFill></fill>';
        }
        $xml .= '</fills>';
        $xml .= '<borders count="' . (count($this->borders) + 1) . '">';
        $xml .= '<border><left/><right/><top/><bottom/><diagonal/></border>';
        foreach ($this->borders as $border) {
            $xml .= $border->getXml();
        }
        $xml .= '</borders>';
        $xml .= '<cellStyleXfs count="1">';
        $xml .= '<xf numFmtId="0" fontId="0" fillId="0" borderId="0"/>';
        $xml .= '</cellStyleXfs>';
        $xml .= '<cellXfs count="' . (count($this->styleSet) + 1) . '">';
        $xml .= '<xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/>';
        foreach ($this->styleSet as $index => $style) {
            $xml .= '<xf numFmtId="' . $style->nformat . '" fontId="' . $style->font
                . '" fillId="' . $style->fill . '" borderId="' . $style->border
                . '" xfId="0" applyBorder="1" applyFill="1"';
            if ($style->alignment) {
                $xml .= ' applyAlignment="1"><alignment';
                foreach ($style->alignment as $type => $value) {
                    $xml .= ' ' . $type . '="' . $value . '"';
                }
                $xml .= '/></xf>';
            } else {
                $xml .= '/>';
            }
        }
        $xml .= '</cellXfs><cellStyles count="1"><cellStyle name="Normal" xfId="0" builtinId="0"/></cellStyles><dxfs count="0"/>'
            . '<tableStyles count="0" defaultTableStyle="TableStyleMedium2" defaultPivotStyle="PivotStyleLight16"/><extLst>'
            . '<ext uri="{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" xmlns:x14="http://schemas.microsoft.com/office/spreadsheetml/2009/9/main">'
            . '<x14:slicerStyles defaultSlicerStyle="SlicerStyleLight1"/></ext></extLst></styleSheet>';
        return $xml;
    }
}