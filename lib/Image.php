<?php
namespace XLSXLight;

use Exception;

class Image
{
    private $id;
    private $name;
    private $originImage;
    private $xlsImage;
    private $rowIndex = 0;
    private $colIndex = 0;
    private $rowOffset = 0;
    private $colOffset = 0;
    private $width;
    private $height;
    private $toRowIndex = 1;
    private $toColIndex = 1;
    private $toColOffset;
    private $toRowOffset;

    /**
     * Image constructor.
     * @param string $cellTag
     * @param string $absPath
     * @throws Exception
     */
    function __construct($cellTag, $absPath)
    {
        if (!file_exists($absPath)) {
            throw new Exception('no file :' . $absPath);
        }
        $this->originImage = $absPath;

        $cellFrom = Workbook::getCellPosition($cellTag);
        $this->rowIndex = $cellFrom['row'] - 1;
        $this->colIndex = $cellFrom['col'];
        //So far on tested images seems like image pixel ratio is 75% from the original
        //this can proof to be different depending from DPI
        $size = getimagesize($absPath);
        $this->width = $size[0];
        $this->height = $size[1];
    }

    public function setOffset($left, $top = null, $right = null, $bottom = null)
    {
        if ($top !== null) {
            $this->rowOffset = $top * 12700;
        }
        if ($right !== null) {
            $this->toColOffset = $right * 12700;
        }
        if ($bottom !== null) {
            $this->toRowOffset = $bottom * 12700;
        }
        if ($left !== null) {
            $this->colOffset = $left * 12700;
        }
        return $this;
    }

    public function getOffset()
    {
        return [
            'left' => ($this->colOffset ? $this->colOffset / 12500 : 0),
            'top' => ($this->rowOffset ? $this->rowOffset / 12500 : 0),
            'right' => ($this->toColOffset ? $this->toColOffset / 12500 : 0),
            'bottom' => ($this->toRowOffset ? $this->toRowOffset / 12500 : 0),
        ];
    }

    public function getOriginImage()
    {
        return $this->originImage;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        if (!$this->name) {
            $this->name = 'Picture ' . $id;
        }
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $fileName
     * @return $this
     */
    public function setSharedImage($fileName)
    {
        $this->xlsImage = $fileName;
        return $this;
    }

    public function getXlsImage()
    {
        return $this->xlsImage;
    }

    public function setSize($width, $height = 'auto')
    {
        if ($width == 'auto' && $height == 'auto') {
            return $this;
        }
        if ($height == 'auto') {
            $ratio = $this->height / $this->width;
            $height = intval($width * $ratio);
        } elseif ($width == 'auto') {
            $ratio = $this->width / $this->height;
            $width = intval($height * $ratio);
        }
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getColIndex()
    {
        return $this->colIndex;
    }

    public function getRowIndex()
    {
        return $this->rowIndex;
    }

    public function setToRowIndex($index)
    {
        $this->toRowIndex = $index;
        return $this;
    }

    public function setToColIndex($index)
    {
        $this->toColIndex = $index;
        return $this;
    }

    public function getToColIndex()
    {
        return $this->toColIndex;
    }

    public function generateXml()
    {
        return '<xdr:twoCellAnchor editAs="absolute">'

            . '<xdr:from>'
            . '<xdr:col>' . $this->colIndex . '</xdr:col>'
            . '<xdr:colOff>' . $this->colOffset . '</xdr:colOff>'
            . '<xdr:row>' . $this->rowIndex . '</xdr:row>'
            . '<xdr:rowOff>' . $this->rowOffset . '</xdr:rowOff>'
            . '</xdr:from>'

            . '<xdr:to>'
            . '<xdr:col>' . $this->toColIndex . '</xdr:col>'
            . '<xdr:colOff>' . $this->toColOffset . '</xdr:colOff>'
            . '<xdr:row>' . $this->toRowIndex . '</xdr:row>'
            . '<xdr:rowOff>' . $this->toRowOffset . '</xdr:rowOff>'
            . '</xdr:to>'

            . '<xdr:pic>'
            . '<xdr:nvPicPr>'
            . '<xdr:cNvPr id="' . $this->id . '" name="' . $this->name . '"/>'
            . '<xdr:cNvPicPr><a:picLocks noChangeAspect="1"/></xdr:cNvPicPr>'
            . '</xdr:nvPicPr>'

            . '<xdr:blipFill>'
            . '<a:blip xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:embed="rId' . $this->id . '">'
            . '<a:extLst>'
            . '<a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">'
            . '<a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>'
            . '</a:ext>'
            . '</a:extLst>'
            . '</a:blip>'
            . '<a:stretch><a:fillRect/></a:stretch>'
            . '</xdr:blipFill>'

            . '<xdr:spPr>'
            . '<a:xfrm>'
            . '<a:off x="0" y="0"/><a:ext cx="0" cy="0"/>'
            . '</a:xfrm>'
            . '<a:prstGeom prst="rect">'
            . '<a:avLst/>'
            . '</a:prstGeom>'
            . '</xdr:spPr>'
            . '</xdr:pic>'

            . '<xdr:clientData/>'
            . '</xdr:twoCellAnchor>';
    }
}