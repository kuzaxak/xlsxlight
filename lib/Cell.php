<?php
namespace XLSXLight;

use DateTime;
use Exception;

class Cell
{
    private $value;
    private $index;
    private $merge;
    private $mergedRows;
    private $mergedColumns;
    private $rowIndex;
    private $colIndex;

    private $font = 'default';
    private $numFormat = null;
    private $fill = 'default';
    private $border = null;

    /**
     * XLSXLightCell constructor.
     * @param string $cellRange
     * @param DateTime|string|int|float|null $value
     * @throws Exception
     */
    function __construct($cellRange, $value = null)
    {
        //preg_match("/^([A-Z]+)([0-9]+)$/", $cellIndex, $match);
        preg_match("/^(([A-Z]+)([0-9]+))[:]?(([A-Z]+)([0-9]+))?$/", $cellRange, $match);
        if (!in_array(sizeof($match), [4, 7])) {
            throw new Exception('Invalid Cell Range "' . $cellRange . '"');
        }
        $this->index = $match[1];
        $this->value = $value;
        if (!is_string($value)) {
            $this->numFormat = 'default';
        } elseif ($value instanceof DateTime) {
            $this->numFormat = 'datetime';
        }
        $this->rowIndex = $match[3];
        $this->colIndex = Workbook::getColumnNumber($match[2]);
        if (sizeof($match) == 7) {
            $this->merge = $cellRange;
            $this->mergedRows = range($this->rowIndex, $match[6]);
            $this->mergedColumns = range($this->colIndex, Workbook::getColumnNumber($match[5]));
        }
    }

    /**
     * @return int
     */
    public function getRow()
    {
        return $this->rowIndex;
    }

    /**
     * @return null|string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return int
     */
    public function getCoulmn()
    {
        return $this->colIndex;
    }

    /**
     * @return DateTime|float|int|string
     */
    public function getValue()
    {
        return $this->value;
    }

    public function getMergeRange()
    {
        return $this->merge;
    }

    public function getMergedRows()
    {
        return $this->mergedRows;
    }

    public function getMergedColumns()
    {
        return $this->mergedColumns;
    }

    /**
     * @param string $font
     * @return $this
     */
    public function setFont($font)
    {
        $this->font = $font;
        return $this;
    }

    /**
     * @return string
     */
    public function getFont()
    {
        return $this->font;
    }

    /**
     * @param string $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->numFormat = $format;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFormat()
    {
        return $this->numFormat;
    }

    /**
     * @param string $fillTag
     * @return $this
     */
    public function setFill($fillTag)
    {
        $this->fill = $fillTag;
        return $this;
    }

    /**
     * @return string
     */
    public function getFill()
    {
        return $this->fill;
    }

    /**
     * @param string $borderTag
     * @return $this
     */
    public function setBorder($borderTag)
    {
        $this->border = $borderTag;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getBorder()
    {
        return $this->border;
    }
}