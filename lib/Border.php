<?php
namespace XLSXLight;

class Border
{
    private $lineStyles = [
        'thin',
        'thick',
    ];

    private $id;

    private $styleLeft;
    private $styleTop;
    private $styleRight;
    private $styleBottom;

    private $colorLeft;
    private $colorTop;
    private $colorRight;
    private $colorBottom;

    function __construct($styles, $colors)
    {
        $style = explode(' ', $styles);
        switch (count($style)) {
            case 1:
                $this->styleLeft
                    = $this->styleTop
                    = $this->styleRight
                    = $this->styleBottom
                    = in_array($styles, $this->lineStyles) ? $styles : null;
                break;
            case 2:
                $this->styleLeft
                    = $this->styleRight
                    = in_array($style[0], $this->lineStyles) ? $style[0] : null;
                $this->styleTop
                    = $this->styleBottom
                    = in_array($style[1], $this->lineStyles) ? $style[1] : null;
                break;
            case 4:
                $this->styleLeft = in_array($style[0], $this->lineStyles) ? $style[0] : null;
                $this->styleTop = in_array($style[1], $this->lineStyles) ? $style[1] : null;
                $this->styleRight = in_array($style[2], $this->lineStyles) ? $style[2] : null;
                $this->styleBottom = in_array($style[3], $this->lineStyles) ? $style[3] : null;
                break;
            default:
                $this->styleLeft
                    = $this->styleTop
                    = $this->styleRight
                    = $this->styleBottom
                    = null;
                break;
        }

        $color = explode(' ', $colors);
        switch (count($color)) {
            case 1:
                $this->colorLeft
                    = $this->colorTop
                    = $this->colorRight
                    = $this->colorBottom
                    = $colors;
                break;
            case 2:
                $this->colorLeft
                    = $this->colorRight
                    = $color[0];
                $this->colorTop
                    = $this->colorBottom
                    = $color[1];
                break;
            case 4:
                $this->colorLeft = $color[0];
                $this->colorTop = $color[1];
                $this->colorRight = $color[2];
                $this->colorBottom = $color[3];
                break;
            default:
                $this->colorLeft
                    = $this->colorTop
                    = $this->colorRight
                    = $this->colorBottom
                    = null;
                break;
        }
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getXML()
    {
        $xml = '<border>';
        if ($this->styleLeft) {
            $color = $this->colorLeft ? '<color rgb="' . $this->colorLeft . '"/>' : '<color auto="1"/>';
            $xml .= '<left style="' . $this->styleLeft . '">' . $color . '</left>';
        } else {
            $xml .= '<left/>';
        }
        if ($this->styleRight) {
            $color = $this->colorRight ? '<color rgb="' . $this->colorRight . '"/>' : '<color auto="1"/>';
            $xml .= '<right style="' . $this->styleRight . '">' . $color . '</right>';
        } else {
            $xml .= '<right/>';
        }
        if ($this->styleTop) {
            $color = $this->colorTop ? '<color rgb="' . $this->colorTop . '"/>' : '<color auto="1"/>';
            $xml .= '<top style="' . $this->styleTop . '">' . $color . '</top>';
        } else {
            $xml .= '<top/>';
        }
        if ($this->styleBottom) {
            $color = $this->colorBottom ? '<color rgb="' . $this->colorBottom . '"/>' : '<color auto="1"/>';
            $xml .= '<bottom style="' . $this->styleBottom . '">' . $color . '</bottom>';
        } else {
            $xml .= '<bottom/>';
        }
        $xml .= '<diagonal/>';
        $xml .= '</border>';

        return $xml;
    }
}